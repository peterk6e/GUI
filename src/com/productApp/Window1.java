package com.productApp;

import com.entity.ProductEntity;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window1 {
    private JPanel window1View;
    private JTextField textFieldId;
    private JButton buttonSearch;
    private JLabel labelId;
    private JLabel labelName;
    private JTextField textName;
    private JTextField textDescription;
    private JTextField textPrice;
    private JLabel labelPrice;
    private JLabel labelDescription;


    public  void createWindow1(){
        //JLabel textLabel = new JLabel("I'm a label in the window 1",SwingConstants.CENTER);
        JFrame frame = new JFrame("window 1");
       // frame.getContentPane().add(textLabel);
        frame.setContentPane(new Window1().window1View);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public Window1(){
        buttonSearch.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                String getValue = textFieldId.getText();
                 System.out.println("get value " + getValue);
                int idProd = Integer.parseInt(getValue);

                DbAccess dba = new DbAccess();
                ProductEntity prod = dba.findProductEntity(idProd);

                if(prod != null) {
                    textName.setText(prod.getName());
                    textDescription.setText(prod.getDescription());
                    textPrice.setText(Double.toString(prod.getPrice()) + " THB");
                }
                else{
                    textName.setText("no product");
                    textDescription.setText("--");
                    textPrice.setText("--");
                }

            }
        });
    }


}




